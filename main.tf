## AWS provider
provider "aws" {
  region = "us-east-1"
  shared_credentials_file = "/root/.aws/credentials"
}

resource "aws_vpc" "cb-test-vpc" {
  cidr_block = "192.168.10.0/24"

  tags {
    Name = 'cb-test-vpc'
  }
}

resource "aws_subnet" "cb-test-sub" {
  cidr_block = "192.168.10.0/24"
  vpc_id = "${aws_vpc.cb-test-vpc.id}"
  availability_zone = "us-east-1a"
}

#Security group for instance
resource "aws_security_group" "cb-test-sg" {
  name = "cb-test-secgrp"
  description = "test security group"
  vpc_id = "${aws_vpc.cb-test-vpc.id}"

#ssh access from anywhere
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

# HTTP access from the VPC
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["192.168.10.0/24"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_instance" "web" {
  ami = "ami-09479453c5cde9639"
  instance_type = "t2.micro"
  key_name = "demo"
  vpc_security_group_ids = ["${aws_security_group.cb-test-sg.id}"]
  subnet_id = "${aws_subnet.cb-test-sub.id}"

  tags {
    Name = "TestInstance"
  }
}