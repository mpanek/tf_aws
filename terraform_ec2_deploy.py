import python_terraform
from python_terraform import *
from common.methods import set_progress

dir = '/root/terraform/'

def create_resource():
    set_progress("get the terraform config directory")
    tf = Terraform(working_dir=dir)

    approve = {"auto-approve": True}

    set_progress("initialize terraform config file")
    tf.init(reconfigure=True)

    tf.plan(no_color=IsFlagged, refresh=True, capture_output=False)

    set_progress(tf.apply(**approve))


    return "SUCCESS","",""